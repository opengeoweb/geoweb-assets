Note: this is a proposal that is just stored here for the time being.

This repository is part of the GeoWeb project. All issues related to GeoWeb will be processed in thus repo.

## General

1. **Issues** can be either user stories or bugs. User stories are composed by domain experts and linked to **Epics**
2. **Epics** are managed by the domain experts, e.g. *timeslider* or *2D visualisation of models*
3. **labels** can be added to **Epics** and to **Issues** and can be used to e.g. assign epics and issues to teams, e.g. *KNMI*, *FMI*, *MET Norway*
4. The status of the issues can be monitored on the issue board.

## Epics
- Create Epics [here](https://gitlab.com/groups/opengeoweb/-/epics)
  - Ensure that the title fits with existing epics and do not overlap
  - Set start- and due dates to make it visible in the [roadmap](https://gitlab.com/groups/opengeoweb/-/roadmap)
  - Use label to assign developer teams if this is known

## Issues 

- Create Issues  [here](https://gitlab.com/groups/opengeoweb/-/issues)
  - Use templates for UserStories or Bugs
  - Assagnee is only used if you are a developer and will implement the issue immediately
  - Milestone is left empty, assigned in sprint planning
  - Weight is left empty (?)
  - Labels
  - Due date is left empty, prioritized by Product Owners with help from the team

### Guidance for reporting issues
- [ ] One bug/wish per issue
- [ ] Check on duplicates
- [ ] Always use a friendly pen

1. Titel: summary of issue, line of text upto apx. 10 words
2. Description: explanation of issue with at least the following
    - [ ] steps to reproduce (bug)
    - [ ] expected and actual result (bug)
3. Label for assigning the issue to a (developer)team (can be done at later stage)
4. Screenshot(s)

### Linking issue to Epic
1.  --> I've not discovered a way to directly add the link to an Epic directly from within the issue-form
2.  Once added to the issue list, you can edit the issue and link it to an Epic


## Labels
- Labels are managed  [here](https://gitlab.com/groups/opengeoweb/-/labels)
  - Consult the project group before doing changes to labels, they are the key to keeping issues organized.
- KNMI, FMI and Met Norway are used to indicate that issue is assigned to a spesific organization.
- APPROVED are used for UserStories. This indicate that PO, testers and developers agree that it is ready for implementation.

In addition to Open and Closed, issues can have the following states
- DOING indicate that an issue is currently being implemented by a developer
- TESTING indicat that functionality is being handled by the test team





