<!---
Always remember:
* Titel: summary of issue, line of text upto apx. 10 words
* One bug/wish per issue
* Check on duplicates
* Always use a friendly pen
--->

### User

(Who needs this? Forecaster?)

### Short description

(Describe the functionality you want)

### Why 

(Why is this needed?)

### Test criteria

* (What to do)
  * (what should have happened)


### Extra information
(describe needed extra information or give links to needed information/documentation)
