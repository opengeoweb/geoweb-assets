<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by "TECH DEBT" label:

- https://gitlab.com/groups/opengeoweb/-/issues/?sort=relative_position&state=opened&label_name%5B%5D=TECH%20DEBT&first_page_size=100

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

(A clear and concise description of what the tech debt is and the reason of being created)

### Impact

(Description of the current or possible impact of this tech debt)


### Proposed solutions
How would you fix it?

(if don't you have any solution in mind, write it: 

This tech debt doesn't have a proposed solution.

If you have solutions in mind, describe it above)


### Extra info and/or screenshots

(Paste any relevant info and/or screenshots)


/label ~TECH DEBT
